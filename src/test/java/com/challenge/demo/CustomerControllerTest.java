package com.challenge.demo;

import com.challenge.demo.controllers.CustomerController;
import com.challenge.demo.dto.CustomerDTO;
import com.challenge.demo.services.CustomerService;
import com.challenge.demo.util.ObjectMock;
import org.junit.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CustomerControllerTest {

    @InjectMocks
    private CustomerController customerController;

    @Mock
    private CustomerService customerService;

    private CustomerDTO customerDTO;

    @Before
    public void init(){
        this.customerDTO = ObjectMock.createMockCustomerDtoCompleted();
    }

    @Test
    public void test_getAllCustomers(){
        CustomerDTO customerDTO = new CustomerDTO();
        Mockito.when(this.customerService.findAll()).thenReturn(Arrays.asList(customerDTO));
        ResponseEntity response = customerController.all();

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(1, ((List <CustomerDTO>)response.getBody()).size());
    }

    @Test
    public void test_saveCustomer(){
        Mockito.when(this.customerService.save(this.customerDTO)).then(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                customerDTO.setDocument_id("123456");
                return customerDTO;
            }
        });

        ResponseEntity response = customerController.create(this.customerDTO);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals("123456", ((CustomerDTO)response.getBody()).getDocument_id() );
    }

    @Test
    public void test_deleteCustomer(){
        Mockito.when(this.customerService.delete("123456")).then(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                return true;
            }
        });

        ResponseEntity response = customerController.delete("123456");

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(true, ((Boolean)response.getBody()).booleanValue() );
    }

    @Test
    public void test_getAllCustomerByZipCode(){

        List<CustomerDTO> customers = new ArrayList<>();
        customers.addAll(Arrays.asList(this.customerDTO));

        String zipCode="99999-999";

        Mockito.when(this.customerService.findAllByZipCode(zipCode)).then(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                return customers;
            }
        });

        ResponseEntity response = customerController.findByZipCode(zipCode);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(1, ((List<CustomerDTO>)response.getBody()).size() );
    }
}
