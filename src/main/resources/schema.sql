CREATE TABLE IF NOT EXISTS Customer (
  document_id INT NOT NULL AUTO_INCREMENT ,
  name VARCHAR(45) NULL ,
  age INTEGER NULL ,
  registration_date DATETIME ,
  last_update_info VARCHAR(45),
  PRIMARY KEY (document_id)
);

CREATE TABLE IF NOT EXISTS Address (
  zip_code VARCHAR(45) ,
  number VARCHAR(45),
  FOREIGN KEY (Customer) REFERENCES Customer(document_id)
);