package com.challenge.demo.converters;

import com.challenge.demo.models.Address;
import com.challenge.demo.dto.AddressDTO;

public class AddressConverter {

    public static Address convertToEntity(final AddressDTO addressDTO){

        final Address address = new Address();

        address.setZipCode(addressDTO.getZipCode());
        address.setNumber(addressDTO.getNumber());

        return address;
    }

    public static AddressDTO convertToDTO(final Address address){

        final AddressDTO addressDTO = new AddressDTO();

        addressDTO.setZip_code(address.getZipCode());
        addressDTO.setNumber(address.getNumber());

        return addressDTO;
    }
}
