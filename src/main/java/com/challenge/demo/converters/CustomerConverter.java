package com.challenge.demo.converters;

import com.challenge.demo.dto.CustomerDTO;
import com.challenge.demo.models.Customer;

import java.util.stream.Collectors;

public class CustomerConverter {

    public static Customer convertToEntity(final CustomerDTO customerDTO){

        final Customer customer = new Customer();

        if(customerDTO.getDocument_id()!=null){
            customer.setDocumentId(customerDTO.getDocument_id());
        }

        customer.setAge(customerDTO.getAge());
        customer.setName(customerDTO.getName());
        customer.setLastUpdateInfo(customerDTO.getLast_update_info());

        customer.setAddresses(customerDTO.getAddressDTOS().stream().map(addressDTO -> AddressConverter.convertToEntity(addressDTO)).collect(Collectors.toSet()));

        return customer;
    }

    public static CustomerDTO convertToDTO(final Customer customer){

        final CustomerDTO customerDTO = new CustomerDTO();

        if(customer.getDocumentId()!=null){
            customerDTO.setDocument_id(customer.getDocumentId());
        }

        customerDTO.setAge(customer.getAge());
        customerDTO.setName(customer.getName());
        customerDTO.setLast_update_info(customer.getLastUpdateInfo());

        customerDTO.setAddressDTOS(customer.getAddresses().stream().map(address -> AddressConverter.convertToDTO(address)).collect(Collectors.toList()));

        return customerDTO;
    }
}
