package com.challenge.demo.util;

import com.challenge.demo.dto.CustomerDTO;
import com.challenge.demo.models.Customer;

public class ObjectMock {

    private ObjectMock(){}

    public static CustomerDTO createMockCustomerDtoCompleted(){
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setAge(2);
        customerDTO.setName("razera");

        return customerDTO;
    }

    public static Customer createMockCustomerCompleted(){
        Customer customer = new Customer();
        customer.setDocumentId("12345");
        customer.setAge(2);
        customer.setName("razera");

        return customer;
    }
}
