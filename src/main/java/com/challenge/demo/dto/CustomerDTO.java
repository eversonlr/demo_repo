package com.challenge.demo.dto;

import java.util.Date;
import java.util.List;

public class CustomerDTO {

    String document_id;
    String name;
    int age;
    Date registration_date;
    String last_update_info;

    List<AddressDTO> addressDTOS;

    public String getDocument_id() {
        return document_id;
    }

    public void setDocument_id(String document_id) {
        this.document_id = document_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getRegistration_date() {
        return registration_date;
    }

    public void setRegistration_date(Date registration_date) {
        this.registration_date = registration_date;
    }

    public String getLast_update_info() {
        return last_update_info;
    }

    public void setLast_update_info(String last_update_info) {
        this.last_update_info = last_update_info;
    }

    public List<AddressDTO> getAddressDTOS() {
        return addressDTOS;
    }

    public void setAddressDTOS(List<AddressDTO> addressDTOS) {
        this.addressDTOS = addressDTOS;
    }
}
