package com.challenge.demo.dto;

public class AddressDTO {

    String zipCode;
    String number;

    public String getZipCode() {
        return zipCode;
    }

    public void setZip_code(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
