package com.challenge.demo.services;

import com.challenge.demo.models.Customer;
import com.challenge.demo.repository.CustomerRepository;
import com.challenge.demo.converters.CustomerConverter;
import com.challenge.demo.dto.CustomerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    public CustomerDTO save(CustomerDTO customerDTO){

        Customer customer = CustomerConverter.convertToEntity(customerDTO);
        customer.setRegistrationDate(new Date());
        customer = customerRepository.save(customer);

        customerDTO.setDocument_id(customer.getDocumentId());
        return customerDTO;
    }

    public CustomerDTO update(String documentId, CustomerDTO customerDTO){

        Optional<Customer> updated = customerRepository.findById(documentId);

        if(updated.isPresent()){

            Customer customer = updated.get();
            customer.setAge(customerDTO.getAge());
            customer.setLastUpdateInfo(customerDTO.getLast_update_info());

            customerRepository.save(customer);
            return customerDTO;
        }

        return null;
    }

    public List<CustomerDTO> findAll(){
        return customerRepository.findAll().stream().
                map(customer -> CustomerConverter.convertToDTO(customer)).
                collect(Collectors.toList());
    }

    public Boolean delete(String documentId){

        Optional<Customer> customer = customerRepository.findById(documentId);

        if(customer.isPresent()){
            customerRepository.delete(customer.get());
            return true;
        }

        return false;
    }

    public List<CustomerDTO> findAllByZipCode(String zipCode){

        List<CustomerDTO> customerDTOS = customerRepository.findByAddressesZipCode(zipCode).
                stream().map(customer -> CustomerConverter.convertToDTO(customer)).
                collect(Collectors.toList());

        return customerDTOS;
    }
}
