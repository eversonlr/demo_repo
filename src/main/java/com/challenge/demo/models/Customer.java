package com.challenge.demo.models;

import org.springframework.data.annotation.Id;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "Customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "document_id")
    String documentId;

    @Column(name = "name")
    String name;

    @Column(name = "age")
    int age;

    @Column(name = "registration_date")
    Date registrationDate;

    @Column(name = "last_update_info")
    String lastUpdateInfo;

    @OneToMany(mappedBy = "customer")
    Set<Address> addresses;

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getLastUpdateInfo() {
        return lastUpdateInfo;
    }

    public void setLastUpdateInfo(String lastUpdateInfo) {
        this.lastUpdateInfo = lastUpdateInfo;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }
}
