package com.challenge.demo.models;

import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(value = "Address")
public class Address {

    @Column(value = "zip_code")
    private String zipCode;

    @Column(value = "number")
    private String number;

    @ManyToOne
    @JoinColumn(name = "documentId")
    private Customer customer;

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
