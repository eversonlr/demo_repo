package com.challenge.demo.controllers;

import com.challenge.demo.dto.CustomerDTO;
import com.challenge.demo.services.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/customer")
public class CustomerController {

    final static Logger logger = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    CustomerService customerService;

    @GetMapping("")
    public ResponseEntity<List<CustomerDTO>> all() {
        return new ResponseEntity<>(customerService.findAll(), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<CustomerDTO> create(@RequestBody CustomerDTO customerDTO) {
        return new ResponseEntity<>(customerService.save(customerDTO), HttpStatus.OK);
    }

    @PutMapping("/{documentId}")
    public ResponseEntity<CustomerDTO> update(@PathVariable("documentId") String documentId, @RequestBody CustomerDTO customerDTO) {

        CustomerDTO updated = customerService.update(documentId, customerDTO);

        if(updated==null) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(updated, HttpStatus.OK);
    }

    @DeleteMapping("/{documentId}")
    public ResponseEntity<Boolean> delete(@PathVariable("documentId") String documentId) {

        Boolean ret = customerService.delete(documentId);

        if(ret){
            return new ResponseEntity<>(true, HttpStatus.OK);
        }

        return new ResponseEntity<>(false, HttpStatus.NO_CONTENT);
    }

    @GetMapping("/address/{zipCode}")
    public ResponseEntity<List<CustomerDTO>> findByZipCode(@PathVariable("zipCode") String zipCode){
        List<CustomerDTO> customers = customerService.findAllByZipCode(zipCode);

        return new ResponseEntity<>(customers, HttpStatus.OK);
    }
}